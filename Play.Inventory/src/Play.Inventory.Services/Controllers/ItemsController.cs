using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Play.Common;
using Play.Inventory.Service.Clients;
using Play.Inventory.Service.Dtos;
using Play.Inventory.Service.Entities;

namespace Play.Inventory.Service.Controllers
{
  [ApiController]
  [Route("items")]
  public class ItemsController : ControllerBase
  {

    private readonly IRepository<InventoryItem> inventoryItemsRepository;

    // private readonly CatalogClient catalogClient;
    private readonly IRepository<CatalogItem> catalogItemRepository;

    public ItemsController(IRepository<InventoryItem> inventoryItemsRepository, IRepository<CatalogItem> catalogItemRepository)
    {
      this.inventoryItemsRepository = inventoryItemsRepository;
      this.catalogItemRepository = catalogItemRepository;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<InventoryItemDto>>> GetAsync(Guid userId)
    {

      if (userId == Guid.Empty)
      {
        return BadRequest();
      }

      // var items = (await inventoryItemsRepository.GetAllAsync(item => item.UserId == userId))
      // .Select(item => item.AsDto());

      // var catalogItems = await catalogClient.GetCatalogItemsAsync();

      var inventoryItemEntities = await inventoryItemsRepository.GetAllAsync(item => item.UserId == userId);
      //getting all catalog item id from inventory db
      var itemsIds = inventoryItemEntities.Select(item => item.CatalogItemId);
      //getting catalog info where ids are in itemsIds
      var catalogItemEntities = await catalogItemRepository.GetAllAsync(item => itemsIds.Contains(item.Id));

      var inventoryItemDtos = inventoryItemEntities.Select(InventoryItem =>
      {
        var catalogItem = catalogItemEntities.Single(catalogItem => catalogItem.Id == InventoryItem.CatalogItemId);
        return InventoryItem.AsDto(catalogItem.Name, catalogItem.Description);
      });

      return Ok(inventoryItemDtos);
    }

    [HttpPost]
    public async Task<ActionResult> PostAsync(GrantItemsDto grantItemsDto)
    {
      var inventoryItem = await inventoryItemsRepository.GetAsync(
                item => item.UserId == grantItemsDto.UserId && item.CatalogItemId == grantItemsDto.CatalogItemId);

      if (inventoryItem == null)
      {
        inventoryItem = new InventoryItem
        {
          CatalogItemId = grantItemsDto.CatalogItemId,
          UserId = grantItemsDto.UserId,
          Quantity = grantItemsDto.Quantity,
          AcquiredDate = DateTimeOffset.UtcNow
        };

        await inventoryItemsRepository.CreateAsync(inventoryItem);
      }
      else
      {
        inventoryItem.Quantity += grantItemsDto.Quantity;
        await inventoryItemsRepository.UpdateAsync(inventoryItem);
      }

      return Ok();
    }
  }
}