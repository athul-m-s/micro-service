namespace Play.Common.Settings
{
  public class MongoDbSettings
  {
    //if we are not updating this value after loads this app
    //public string Host { get; init; }
    //init prevents the updation

    //These are the values getting from appsettings.json
    public string Host { get; set; }
    public string Port { get; set; }
    public string ConnectionString => $"mongodb://{Host}:{Port}";
  }
}