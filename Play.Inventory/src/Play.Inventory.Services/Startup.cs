using System;
using System.Net.Http;
using Amazon.Runtime.Internal.Util;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Play.Common.MassTransit;
using Play.Common.MongoDB;
using Play.Inventory.Service.Clients;
using Play.Inventory.Service.Entities;
using Polly;
using Polly.Timeout;

namespace Play.Inventory.Services
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services
            .AddMongo()
            .AddMongoRepository<InventoryItem>("inventoryitems")
            .AddMongoRepository<CatalogItem>("catalogitems")
            .AddMassTransitWithRabbitMq();

            //extracted method
            AddCatalogClient(services);

            services.AddControllers(options =>
            {
                options.SuppressAsyncSuffixInActionNames = false;
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Play.Inventory.Services", Version = "v1" });
            });
        }

        private static void AddCatalogClient(IServiceCollection services)
        {
            services.AddHttpClient<CatalogClient>(client =>
            {
                client.BaseAddress = new Uri("https://localhost:5001");
            })
                        .AddTransientHttpErrorPolicy(builder => builder.Or<TimeoutRejectedException>().WaitAndRetryAsync(
                            5, //retry 5 times
                               //1st req wait for 2sec 2nd 4 etc 3rd 8 sec
                            retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                            onRetry: (outcome, timeSpan, retryAttempt) =>
                            {
                                //triggers on every retry
                                var serviceProvider = services.BuildServiceProvider();
                                serviceProvider.GetService<ILogger<CatalogClient>>()?
                                .LogWarning($"Delayed for {timeSpan.TotalSeconds} seconds, then making retry {retryAttempt}");
                            }
                        ))
                        .AddTransientHttpErrorPolicy(builder => builder.Or<TimeoutRejectedException>().CircuitBreakerAsync(
                            3, //after how much error or retries circuit break should open
                            TimeSpan.FromSeconds(15),//how much time the circuit breaker should be in open state
                            onBreak: (outCome, timeSpan) =>
                            {
                                var serviceProvider = services.BuildServiceProvider();
                                serviceProvider.GetService<ILogger<CatalogClient>>()?
                                .LogWarning($"Opening the circuit for {timeSpan.TotalSeconds} seconds...");
                            },
                            onReset: () =>
                            {
                                var serviceProvider = services.BuildServiceProvider();
                                serviceProvider.GetService<ILogger<CatalogClient>>()?
                                .LogWarning($"Closing the circuit...");
                            }
                        ))
                        .AddPolicyHandler(Polly.Policy.TimeoutAsync<HttpResponseMessage>(1));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Play.Inventory.Services v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
