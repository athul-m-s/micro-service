using System;

namespace Play.Inventory.Service.Dtos
{
  public record GrantItemsDto(Guid UserId, Guid CatalogItemId, int Quantity);

  public record InventoryItemDto(Guid CatalogItemId, string Name, string Description, int Quantity, DateTimeOffset AcquiredDate);

  //for inventory price and date details not required
  // public record CatalogItemDto(Guid Id, string Name, string Description, decimal Price, DateTimeOffset CreatedDate);
  public record CatalogItemDto(Guid Id, string Name, string Description);

}